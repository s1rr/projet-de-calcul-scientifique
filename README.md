# Numerical resolution of PDE by gradient descent

Approach of a partial differential equation by triangulation, numerical solution by gradient descent, implementation in C++.
