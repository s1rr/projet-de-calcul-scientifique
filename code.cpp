#include <stdio.h>
#include<iostream>
#include<vector>
#include<cmath>
#include <functional>
using namespace std;

int print_vector(vector<double> vect); // affichage matrice
int print_matrix(vector<vector<double>> M);
void unifdiv(double a, int N, vector<double> &xi); // numérotation des noeuds
int numgb(int N, int M, int i, int j, int &s);
int invnumgb(int N, int M, int s, int &i, int &j);
int numint(int N, int M, int i, int j, int &k);
int invnumint(int N, int M, int k, int &i, int &j);
void num_int_gb(int N, int M, int k, int &s);
void num_gb_int(int N, int M, int s, int &k);
void maillageTR(int N, int M, vector<vector<double>> &TRG); // triangulation
void CalcMatBT(vector<double> xs,vector<double> ys,vector<vector<double>> &BT); // question 2.11
void GradGrad(vector<double> xs, vector<double> ys, vector<vector<double>> &GrdGrd); // question 2.16
vector<double> lambda(vector<double> xs, vector<double> ys, double x, double y); // question 2.17 c
double coef_c(double x, double y);
void INTEGWW(vector<double> xs, vector<double> ys, vector<vector<double>> &CWW, double (*coef_c)(double, double));
int DansTrg(vector<double> xs, vector<double> ys, double x, double y); // question 2.19
int extendVec(int N, int M, vector<double> V_int, vector<double> &V_maillage); // question 3.6
int IntVec(int N, int M, vector<double> V_maillage, vector<double> &V_int);
int matvec(vector<double> V, vector<double> &W, int N, int M, double (*coef_c)(double, double)); // question 7
int addition_matrix(vector<vector<double>> &A, vector<vector<double>> &B, vector<vector<double>> &res);
int scdmembre(vector<double> V, vector<double> &B , int N, int M); // question 3.8
 double rhsf(double x, double y);
int FT(double xref, double yref, vector<double> xs, vector<double> ys, vector<double> &res);
int INTEGSCD(vector<double> xs, vector<double> ys, double i, double &res);
double normL2Grad(vector<double> V, vector<double> W, int N, int M);  // question 3.10
double coef_c_normL2Grad(double x, double y);
double normL2(vector<double> V, vector<double> &W, int N, int M);  // question 3.12
double coef_c_normL2(double x, double y);
int gradient_conjugue(vector<double> &X, int N, int M); // Partie 4
int gradient_G (vector<double> V, vector<double> &W, int N, int M);
int prod_lambda_y(vector<double> X, vector<double> Y, vector<double> &lambdaY, int N, int M);
int prod_beta_y(vector<double> Xn1, vector<double> Y, vector<double> &betaY, int N, int M);
int prod_tYAY(vector<double> Y, double &tyay, int N, int M);
double rhsf (double x, double y); // Partie 5
double coef_c_test(double x, double y);
double solExa(double x, double y, double k0);
double sol_interpole(double x, double y, double k0, int N, int M);
int erreurs (double (*solExa)(double, double, double), vector<double> &erreurs_relatives);

/*      */
/* MAIN */
/*      */

int main() {
    // vector<double> erreurs_relatives(3, 0);
    // erreurs (solExa, erreurs_relatives);
    // print_vector(erreurs_relatives);
    return 0;
}


/*           */
/* FUNCTIONS */
/*           */

int print_vector(vector<double> vect) {
    for (int i = 0; i < vect.size() ; i++) {
	cout << vect[i] << " ";
    }
    cout << endl << endl;
    return 0;
}

int print_matrix(vector<vector<double>> M) {
    for (int i = 0; i < M.size(); i++) {
	for (int j = 0; j < M[0].size() ; j++) {
	    cout << M[i][j] << " ";
	}
	cout << endl;
    }
    cout << endl;
    return 0;
}

void unifdiv(double a, int N, vector<double> &xi){
    for(int i = 0; i < N + 1; i++) {
	xi[i] = i * a / N;
    }
}

int numgb(int N,int M,int i,int j,int &s){
    if ((j>M) or (i>N)) {
		return 1;
    }
    s = (N+1)*j+i;
    return 0;
}

int invnumgb(int N, int M, int s, int &i, int &j){
	if (s > ((N+1) * (M+1) - 1)) {
		return 1;
	}
	i = s % (N+1);
	j = s / (N+1);
	return 0;
}

int numint(int N, int M, int i, int j, int &k){
	if (j>M-1 or i>N-1){
		return 1;
	}
	k=(j-1)*(N-1)+i-1;
	return 0;
}

int invnumint(int N, int M, int k, int &i, int &j){
	if (k>((N-1)*(M-1)-1)){
		return 1;
	}
	i=k%(N-1)+1;
	j=k/(N-1)+1;
	return 0;
}

void num_int_gb(int N, int M, int k, int &s){
	int i,j;
	invnumint(N, M, k, i, j);
	numgb(N, M, i, j, s);
}


void num_gb_int(int N, int M, int s, int &k){
    int i,j;
    invnumgb(N,M,s,i,j);
    numint(N,M,i,j,k);
}

void maillageTR(int N, int M, vector<vector<double>> &TRG){
    int n,i,j,s,l;
    for (l=0;l<N*M*2;l++) {
	n=l/2;
	i=n%N;
	j=n/N;
	if (l%2==0) {
	    if (i%2==j%2){
		numgb(N,M,i,j,s);
		TRG[l][0]=s;
		numgb(N,M,i+1,j,s);
		TRG[l][1]=s;
		numgb(N,M,i+1,j+1,s);
		TRG[l][2]=s;
	    }
	    if (i%2!=j%2) {
		numgb(N,M,i,j,s);
		TRG[l][0]=s;
		numgb(N,M,i+1,j,s);
		TRG[l][1]=s;
		numgb(N,M,i,j+1,s);
		TRG[l][2]=s;
	    }
	}
	if (l%2==1) {
	    if (i%2==j%2) {
		numgb(N,M,i,j,s);
		TRG[l][0]=s;
		numgb(N,M,i,j+1,s);
		TRG[l][1]=s;
		numgb(N,M,i+1,j+1,s);
		TRG[l][2]=s;
	    }
	    if (i%2!=j%2) {
		numgb(N,M,i+1,j,s);
		TRG[l][0]=s;
		numgb(N,M,i,j+1,s);
		TRG[l][1]=s;
		numgb(N,M,i+1,j+1,s);
		TRG[l][2]=s;
	    }
	}
    }
}

void CalcMatBT(vector<double> xs,vector<double> ys,vector<vector<double>> &BT){
	BT[0][0] = xs[1] - xs[0];
	BT[0][1] = xs[2] - xs[0];
	BT[1][0] = ys[1] - ys[0];
	BT[1][1] = ys[2] - ys[0];
}

void GradGrad(vector<double> xs, vector<double> ys, vector<vector<double>> &GrdGrd){
    vector<vector<double>> BT(2, vector<double>(2, 0));
    CalcMatBT(xs,ys,BT); /*je commence par calculer BT */

    vector<vector<double>> BTinv(2, vector<double>(2, 0));
    BTinv[0][0]=BT[1][1]; /*je calcule l'inverse de BT */
    BTinv[0][1]=-BT[0][1];
    BTinv[1][0]=-BT[1][0];
    BTinv[1][1]=BT[0][0];

    /*je calcule tous les gradients des fonctions barycentriques*/
    vector<vector<double>> gradlambda {{-1, -1},
				       {1, 0},
				       {0, 1}};
    /*je cherche le determinant de BT */
    double detBT=fabs(BT[0][0]*BT[1][1] - BT[0][1]*BT[1][0]);
    /*je calcule les coefficients de GrdGrd */
    for (int i=0; i<3; i++){
	for (int k=0; k<3; k++){
	    GrdGrd[i][k]=0.5/detBT*((gradlambda[i][0]*BTinv[0][0]+BTinv[1][0]*gradlambda[i][1])*(gradlambda[k][0]*BTinv[0][0]+BTinv[1][0]*gradlambda[k][1])+((gradlambda[i][0]*BTinv[0][1]+BTinv[1][1]*gradlambda[i][1])*(gradlambda[k][0]*BTinv[0][1]+BTinv[1][1]*gradlambda[k][1])));
	};
    };
}

vector<double> lambda(vector<double> xs, vector<double> ys, double x, double y){
    vector<double> reslambda(3, 0);
    /*je commence par calculer BT */
    vector<vector<double>> BT(2);
    BT[0].resize(2);
    BT[1].resize(2);
    CalcMatBT(xs,ys,BT);
    /*je calcule l'inverse de BT */
    vector<vector<double>> BTinv(2);
    BTinv[0].resize(2);
    BTinv[1].resize(2);
    BTinv[0][0]=BT[1][1];
    BTinv[0][1]=-BT[0][1];
    BTinv[1][0]=-BT[1][0];
    BTinv[1][1]=BT[0][0];
    /*je cherche le determinant de BT */
    double detBT=fabs(BT[0][0]*BT[1][1] - BT[0][1]*BT[1][0]);
    reslambda[1] =(1/detBT)*(BTinv[0][0]*(x-xs[0]) + BTinv[0][1]*(y-ys[0]));
    reslambda[2] =(1/detBT)*(BTinv[1][0]*(x-xs[0]) + BTinv[1][1]*(y-ys[0]));
    reslambda[0]=1-reslambda[1]-reslambda[2];
    return reslambda;
}

double coef_c(double x, double y){
	return x+y;
}

void INTEGWW(vector<double> xs, vector<double> ys, vector<vector<double>> &CWW, double (*coef_c)(double, double)){
	for (int i = 0; i < 3 ; i++) {
		for (int j = 0 ; j < 3 ; j++) {
			CWW[i][j]  = -9/32.0 * coef_c(1.0/3.0,1.0/3.0)*lambda(xs,ys,1.0/3.0,1.0/3.0)[i]*lambda(xs,ys,1.0/3.0,1.0/3.0)[j];
			CWW[i][j] += 25/96.0 * coef_c(0.2,0.6)*lambda(xs,ys,0.2,0.6)[i]*lambda(xs,ys,0.2,0.6)[j];
			CWW[i][j] += 25/96.0 * coef_c(0.2,1.0/15.0)*lambda(xs,ys,0.2,0.2)[i]*lambda(xs,ys,0.2,1.0/15.0)[j];
			CWW[i][j] += 25/96.0 * coef_c(0.6,0.2)*lambda(xs,ys,0.6,0.2)[i]*lambda(xs,ys,0.6,0.2)[j];
		}
	}
}


int DansTrg(vector<double> xs, vector<double> ys, double x, double y){
    vector<double> reslambda(3);
    reslambda[0]=lambda(xs,ys,x,y)[0];
    reslambda[1]=lambda(xs,ys,x,y)[1];
    reslambda[2]=lambda(xs,ys,x,y)[2];
    if (reslambda[0]<0 or reslambda[1]<0 or reslambda[2]) {return 1;}
    return 0;
}

/*          */
// PARTIE 3 //
/*          */

// QUESTION 6
int extendVec(int N, int M, vector<double> V_int, vector<double> &V_maillage) {
    if (V_int.size() != (N - 1) * (M - 1) or V_maillage.size() != (N + 1) * (M + 1)) {
	cout << "Wrong size." << endl;
	return 1;
    }
    int j = 0;
    for (int i = 0; i < (M + 1) * (N + 1); i++) { // bord haut et bas
	if ((i <= N) or (i >= (N + 1) * M)) {
	    V_maillage[i] = 0;
	}
	else if (((i + 1) % (N + 1)) == 0 or (i % (N + 1)) == 0) { // bord gauche et droite
	    V_maillage[i] = 0;
	}
	else {
	    V_maillage[i] = V_int[j];
	    j += 1;  // On pourrait utiliser num_gb_int ici à la place d'un j qu'on incrémente
	}
    }
    return 0;
}

int IntVec(int N, int M, vector<double> V_maillage, vector<double> &V_int) {
    if (V_int.size() != (N - 1) * (M - 1) or V_maillage.size() != (N + 1) * (M + 1)) {
	cout << "Wrong size." << endl;
	return 1;
    }
    int j = 0;
    for (int i = 0; i < (M + 1) * (N + 1); i++) {
	if ((i <= N) or (i >= (N + 1) * M)) { // bord haut et bas
	} // on fait rien
	else if (((i + 1) % (N + 1)) == 0 or (i % (N + 1)) == 0) {  // bord gauche et droite
	} // on fait rien
	else {
	    V_int[j] = V_maillage[i];
	    j += 1;
	}
    }
    return 0;
}

// QUESTION 7
int matvec(vector<double> V, vector<double> &W, int N, int M, double (*coef_c)(double, double)) {
    vector<double> VV ((N + 1) * (M + 1), 0); // vecteur global VV
    vector<double> WW ((N + 1) * (M + 1), 0);
    extendVec(N, M, V, VV);
    vector<vector<double>> BT (2, vector<double>(2, 0));
    double res = 0;
    vector<vector<double>> TRG (2*M*N, vector<double>(3, 0));
    maillageTR(N, M, TRG); // liste des triangles avec les numéros globaux de leurs sommets
    vector<double> xs(3, 0);
    vector<double> ys(3, 0);
    vector<int> xsint(3, 0); // probleme de typage de invnumgb
    vector<int> ysint(3, 0);
    int s = 0;
    int r = 0;
    vector<vector<double>> GrdGrd (3, vector<double>(3, 0));
    vector<vector<double>> CWW (3, vector<double>(3, 0));
    vector<vector<double>> PROD2 (3, vector<double>(3, 0));

    for (int t = 0; t < 2*N*M; t++) { // numéros triangles
	invnumgb(N, M, TRG[t][0], xsint[0], ysint[0]); // retourne les coord des sommets globaux du triangle
	invnumgb(N, M, TRG[t][1], xsint[1], ysint[1]);
	invnumgb(N, M, TRG[t][2], xsint[2], ysint[2]);
	for (int i = 0; i <= 2; i++) { // probleme de typage de invnumgb
	    xs[i] = xsint[i];
	    ys[i] = ysint[i];
	}

	// On stock dans xs, ys les coordonnées des trois sommets du triangle t

	GradGrad(xs, ys, GrdGrd);
	INTEGWW(xs, ys, CWW, coef_c);
	addition_matrix(GrdGrd, CWW, PROD2);
	// On obtient une matrice 3x3 dont les elements sont les resultats des combinaisons des 3 sommets de t

	for (int i = 0; i <= 2; i++) {
	    s = TRG[t][i];
	    res = 0;
	    for (int j = 0; j <= 2; j++) {
		r = TRG[t][j];
		res += VV[r] * PROD2[i][j];
	    }
	    WW[s] += res; // somme sur le sommet i
	}
    }
    IntVec(N, M, WW, W);
    return 0;
}

int addition_matrix(vector<vector<double>> &A, vector<vector<double>> &B, vector<vector<double>> &res) {
    int n = A.size();
    for (int i = 0; i < n ; i++) {
	for (int j = 0; j < n ; j++) {
	    res[i][j] = A[i][j] + B[i][j];
	}
    }
    return 0;
}


// QUESTION 8
int scdmembre(vector<double> V, vector<double> &B , int N, int M) {
    vector<double> VV ((N + 1) * (M + 1), 0); // vecteur global VV
    vector<double> BB ((N + 1) * (M + 1), 0);
    extendVec(N, M, V, VV);
    vector<vector<double>> BT (2, vector<double>(2, 0));
    double res = 0;
    vector<vector<double>> TRG (2*M*N, vector<double>(3, 0));
    maillageTR(N, M, TRG); // liste des triangles avec les numéros globaux de leurs sommets
    int s = 0;
    vector<double> xs(3, 0);
    vector<double> ys(3, 0);
    vector<int> xsint(3, 0); // probleme de typage de invnumgb
    vector<int> ysint(3, 0);
    vector<int> tmp(2, 0);

    for (int t = 0; t < 2*N*M; t++) { // numéros triangle
	invnumgb(N, M, TRG[t][0], xsint[0], ysint[0]); // retourne les coord des sommets globaux du triangle
	invnumgb(N, M, TRG[t][1], xsint[1], ysint[1]);
	invnumgb(N, M, TRG[t][2], xsint[2], ysint[2]);
	// On stock dans xs, ys les coordonnées des trois sommets du triangle t
	for (int i = 0; i <= 2; i++) {
	    xs[i] = xsint[i];
	    ys[i] = ysint[i];
	}

	for (int i = 0; i <= 2; i++) {
	    // On doit mettre le sommet M_k^i en x_0, y_0
	    if (i != 0) {
		tmp[0] = xs[0]; tmp[1] = ys[0];
		xs[0] = xs[i]; ys[0] = ys[i];
		xs[i] = tmp[0]; ys[i] = tmp[1];
	    }

	    s = TRG[t][i];
	    INTEGSCD(xs, ys, double(i), res);
	    BB[s] += res;
	}
    }
    IntVec(N, M, BB, B);
    return 0;
}

// double rhsf(double x, double y) {
//     return x + y;
// }


int FT(double xref, double yref, vector<double> xs, vector<double> ys, vector<double> &res) {
    vector<vector<double>> BT(2, vector<double>(2, 0));
    CalcMatBT(xs, ys, BT);
    res[0] = BT[0][0] * xref + BT[0][1] * yref;
    res[1] = BT[1][0] * xref + BT[1][1] * yref;
    res[0] += xs[0];
    res[1] += ys[0];
    return 0;
}

int INTEGSCD(vector<double> xs, vector<double> ys, double i, double &res){
    vector<double> ft_res(2, 0);
    FT(1.0/3.0, 1.0/3.0, xs, ys, ft_res);
    res =  -9/32.0 * rhsf(ft_res[0], ft_res[1]) * (1 - 1.0/3.0 - 1.0/3.0); // lambda_0(x^, y^)
    FT(1.0/5.0, 3.0/5.0, xs, ys, ft_res);
    res += 25/96.0 * rhsf(ft_res[0], ft_res[1]) * (1 - 1.0/5.0 - 1.0/5.0);
    FT(1.0/15.0, 1.0/5.0, xs, ys, ft_res); // on échange 1/5 et 1/15 due a une erreur d'enoncé
    res += 25/96.0 * rhsf(ft_res[0], ft_res[1]) *(1 - 1.0/15.0 - 1.0/5.0);
    FT(3.0/5.0, 1.0/5.0, xs, ys, ft_res);
    res += 25/96.0 * rhsf(ft_res[0], ft_res[1]) * (1 - 3.0/5.0 - 1.0/5.0);

    vector<vector<double>> BT(2, vector<double>(2, 0));
    CalcMatBT(xs, ys, BT);
    double detBT = fabs(BT[0][0] * BT[1][1] - BT[0][1] * BT[1][0]); // déterminant de BT
    res = detBT * res;
    return 0;
}

// QUESTION 10
double coef_c_normL2Grad(double x, double y) { return 0; } // c_norml2grad

double normL2Grad(vector<double> V, int N, int M) {
    double res = 0;
    vector<double> W ((N - 1) * (M - 1), 0);
    matvec(V, W, N, M, coef_c_normL2Grad);
    for (int i = 0; i < (N - 1) * (M - 1); i++) {
	res += W[i] * W[i];
    }
    return res;
}

// QUESTION 12
double coef_c_normL2 (double x, double y) { return 1; } // c_norml2

double normL2(vector<double> V, int N, int M) {
    vector<double> W ((N - 1) * (M - 1), 0);
    vector<double> VV ((N + 1) * (M + 1), 0); // vecteur global VV
    vector<double> WW ((N + 1) * (M + 1), 0); // vecteur global WW
    extendVec(N, M, V, VV);
    vector<vector<double>> BT (2, vector<double>(2, 0));
    double res = 0;
    vector<vector<double>> TRG (2*M*N, vector<double>(3, 0));
    maillageTR(N, M, TRG); // liste des triangles avec les numéros globaux de leurs sommets
    vector<double> xs(3, 0);
    vector<double> ys(3, 0);
    vector<int> xsint(3, 0); // on résoud un probleme de typage, xs int
    vector<int> ysint(3, 0);
    int s = 0;
    int r = 0;
    vector<vector<double>> PROD2 (3, vector<double>(3, 0));

    for (int t = 0; t < 2*N*M; t++) { // numéros triangles
	invnumgb(N, M, TRG[t][0], xsint[0], ysint[0]); // retourne les coord des sommets globaux du triangle
	invnumgb(N, M, TRG[t][1], xsint[1], ysint[1]);
	invnumgb(N, M, TRG[t][2], xsint[2], ysint[2]);
	// On stock dans xs, ys les coordonnées des trois sommets du triangle t
	for (int i = 0; i <= 2; i++) {
	    xs[i] = xsint[i];
	    ys[i] = ysint[i];
	}

	INTEGWW(xs, ys, PROD2, coef_c_normL2); // intégrale de lambda_s et lambda_r

	for (int i = 0; i <= 2; i++) {
	    s = TRG[t][i];
	    res = 0;
	    for (int j = 0; j <= 2; j++) {
		r = TRG[t][j];
		res += VV[r] * VV[s] * PROD2[i][j]; // v_r * v_s * intégrale
	    }
	    WW[s] += res; // somme sur le sommet i
	}
    }
    res = 0;
    IntVec(N, M, WW, W);
    for (int i = 0; i < (N - 1) * (M - 1); i++) {
	res += W[i];
    }
    return res;
}

/*          */
// PARTIE 4 //
/*          */

int gradient_conjugue(vector<double> &X, int N, int M) {
    int I = (N - 1) * (M - 1);

    vector<double> Y (I, 0);
    vector<double> lambdaY(I, 0);
    vector<double> betaY(I, 0);
    vector<double> vect_gradient(I, 0);
    gradient_G(X, vect_gradient, N, M); // vect_gradient prend la valeur gradient de G(X0)
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // Y = - gradient de G(X0)
	Y[i] = - vect_gradient[i];
    }

    for (int i = 0; i < I; i++) {
	cout << "Etape méthode du gradient conjugué:" << i << " / " << I << endl;
	// Xk+1
	prod_lambda_y(X, Y, lambdaY, N, M); // output: lambdaY = lambda * Yk
	for (int j = 0; j < I; j++) { // Xk+1
	    X[j] = X[j] + lambdaY[j];
	}

	// Yk+1
	gradient_G(X, vect_gradient, N, M); // vect_gradient prend la valeur gradient de G(Xk+1)
	prod_beta_y(X, Y, betaY, N, M);  // output: betaY = beta * Yk

	for (int j = 0; j < I; j++) { // Yn+1
	    Y[j] = - vect_gradient[j] + betaY[j];
	}

    }

    return 0;
}

int gradient_G (vector<double> V, vector<double> &W, int N, int M) {
// nabla G = AU - B
    vector<double> B((N - 1) * (M - 1), 0);
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // on met le vecteur gradient a 0
	W[i] = 0;
    }
    scdmembre(V, B, N, M);
    matvec(V, W, N, M, coef_c_test);
    for (int i = 0; i < (N - 1) * (M - 1); i++) {
	W[i] = W[i] - B[i];
    }
    return 0;
}

int prod_lambda_y(vector<double> X, vector<double> Y, vector<double> &lambdaY, int N, int M) {
    double lambda = 0;
    double tyay = 0;
    vector<double> vect_gradient((N - 1) * (M - 1), 0);

    gradient_G(X, vect_gradient, N, M);
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // lambda = nablaG(X)^t.Y
	lambda += vect_gradient[i] * Y[i];
    }
    prod_tYAY(Y, tyay, N, M);
    lambda = - lambda / tyay;
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // lambdaY = lambda * Y
	lambdaY[i] = lambda * Y[i];
    }
    return 0;
}

int prod_beta_y(vector<double> Xn1, vector<double> Y, vector<double> &betaY, int N, int M) {
    double beta = 0;
    double tyay = 0;
    vector<double> AY ((N - 1) * (M - 1), 0);
    vector<double> vect_gradient((N - 1) * (M - 1), 0);
    matvec(Y, AY, N, M, coef_c_test); // AY

    gradient_G(Xn1, vect_gradient, N, M);
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // beta = nablaG(X)^t.Y
	beta += vect_gradient[i] * AY[i];
    }
    prod_tYAY(Y, tyay, N, M);
    beta = beta / tyay;
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // lambdaY = lambda * Y
	betaY[i] = beta * Y[i];
    }
    return 0;
}

int prod_tYAY(vector<double> Y, double &tyay, int N, int M) {
    vector<double> AY ((N - 1) * (M - 1), 0);
    matvec(Y, AY, N, M, coef_c_test);
    for (int i = 0; i < (N - 1) * (M - 1); i++) { // lambda = nablaG(X)^t.Y
	tyay += Y[i] * AY[i];

    }
    return 0;
}


/*                            */
// PARTIE 5 - TEST NUMERIQUES //
/*                            */

double rhsf (double x, double y) {
    double pi = 3.1415926535;
    double k0 = 1;
    return (2 * k0 * k0 * pi * pi + coef_c_test(x, y)) * sin(k0 * pi * x) * sin(k0 * pi * y);
}

double coef_c_test(double x, double y) {
    double res = 0;
    res = 2 - cos(2 * x + 3 * y);
    return res;
}

double solExa(double x, double y, double k0) {
    double pi = 3.1415926535;
    double res = 0;
    res = sin(k0 * pi * x) * sin(k0 * pi * y);
    return res;
}


int erreurs (double (*solExa)(double, double, double), vector<double> &erreurs_relatives) {
    int N = 10;
    int M = N;
    vector<double> vect_sol_interpole ((N - 1) * (M - 1), 0);
    vector<double> vect_sol_systeme ((N - 1) * (M - 1), 0);
    vector<double> vect_sous ((N - 1) * (M - 1), 0); // soustraction
    int k = 0;
    double Mx = 0;
    double My = 0;
    double normeinf = 0;

    for (double i = 1; i < M; i++) {
	for (double j = 1; j < N; j++) {
	    Mx = j / N;
	    My = i / M;
	    vect_sol_interpole[k] = solExa(Mx, My, 1.0);
	    k += 1;
	}
    }
    gradient_conjugue(vect_sol_systeme, N, M);
    cout << "Vecteur extrapolé: " << endl; // DEBUG
    print_vector(vect_sol_interpole);
    cout << "Vecteur solution du systeme par la méthode du gradient: " << endl;
    print_vector(vect_sol_systeme);
    for (int i = 0; i < (N - 1) * (M - 1) ; i++) {
	vect_sous[i] = vect_sol_interpole[i] - vect_sol_systeme[i];
    }

    cout << "Vecteur soustrait: " << endl; // DEBUG
    print_vector(vect_sous);

    erreurs_relatives[0] = normL2(vect_sous, N, M) / normL2(vect_sol_interpole, N, M);
    erreurs_relatives[1] = normL2Grad(vect_sous, N, M) / normL2Grad(vect_sol_interpole, N, M);
    erreurs_relatives[2] = fabs(solExa(1, 1, 1));
    for (double i = 1; i < M; i++) {
	for (double j = 2; j < N; j++) {
	    Mx = j / N;
	    My = i / M;
	    normeinf = fabs(solExa(Mx, My, 1));
	    if (normeinf > erreurs_relatives[2]) {
		erreurs_relatives[2] = normeinf;
	    }
	    k += 1;
	}
    }

    return 0;
}
